/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.repository;

import br.com.redeuze.crm.entity.Providencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan
 */

@Repository
public interface ProvidenciaRepository extends JpaRepository<Providencia, Long>{
    
}
