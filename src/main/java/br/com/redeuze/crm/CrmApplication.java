package br.com.redeuze.crm;

import br.com.redeuze.crm.entity.Funcionalidade;
import br.com.redeuze.crm.entity.Lotacao;
import br.com.redeuze.crm.entity.Perfil;
import br.com.redeuze.crm.entity.Rede;
import br.com.redeuze.crm.entity.Status;
import br.com.redeuze.crm.entity.Usuario;
import br.com.redeuze.crm.repository.FuncionalidadeRepository;
import br.com.redeuze.crm.repository.LotacaoRepository;
import br.com.redeuze.crm.repository.PerfilRepository;
import br.com.redeuze.crm.repository.RedeRepository;
import br.com.redeuze.crm.repository.StatusRepository;
import br.com.redeuze.crm.repository.UsuarioRepository;


import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@SpringBootApplication
public class CrmApplication implements CommandLineRunner{

    @Autowired
    private RedeRepository redeRepo;
    
    @Autowired
    private LotacaoRepository lotacaoRepo;
    
    @Autowired
    private StatusRepository statusRepo;
    
    @Autowired
    private FuncionalidadeRepository funcionalidadeRepo;
    
    @Autowired
    private PerfilRepository perfilRepo;
    
    @Autowired
    private UsuarioRepository usuarioRepo;
    
    
    @Autowired
    private BCryptPasswordEncoder encode;
    
    
    
    	public static void main(String[] args) {
		SpringApplication.run(CrmApplication.class, args);
	}
        
        @Override
        public void run(String... args) throws Exception {

                Rede rede1 = new Rede(null, "Big Master");
                Rede rede2 = new Rede(null, "Trampolim");
                Rede rede3 = new Rede(null, "Fanglolândia");
                Rede rede4 = new Rede(null, "TRIGG");
                redeRepo.saveAll(Arrays.asList(rede1,rede2,rede3,rede4));
            
                
                
                
                Lotacao lotacao1 = new Lotacao(null, "Central de Atendimento", "Central");
                Lotacao lotacao2 = new Lotacao(null, "Produto", "Produto");
                Lotacao lotacao3 = new Lotacao(null, "Cobrança", "Cobrança");
                Lotacao lotacao4 = new Lotacao(null, "Mesa de Crédito", "Mesa de Credito");
                lotacaoRepo.saveAll(Arrays.asList(lotacao1,lotacao2,lotacao3,lotacao4));
                
                
                
                Status status2 = new Status(2l, "Encerrada");
                Status status1 = new Status(1l, "Pendente");
                Status status3 = new Status(3l, "Em andamento");
                Status status4 = new Status(3l, "Nova");
                statusRepo.saveAll(Arrays.asList(status1,status2,status3,status4));
    
                
                
                Funcionalidade f1 = new Funcionalidade(null, "cadastrar funcionario", "cadastro");
                Funcionalidade f2 = new Funcionalidade(null, "Consultar funcionario", "consulta");
                Funcionalidade f3 = new Funcionalidade(null, "Editar funcionario", "edita");
                Funcionalidade f4 = new Funcionalidade(null, "Excluir funcionario", "exclui");
                Funcionalidade f5 = new Funcionalidade(null, "Relatorio funcionario", "relatorio");
                funcionalidadeRepo.saveAll(Arrays.asList(f1,f2,f3,f4,f5));
                
                
                
               
                Perfil p1 = new Perfil(null, "ADMIN", null);
                p1.getFuncionalidades().add(f1);
                p1.getFuncionalidades().add(f2);
                p1.getFuncionalidades().add(f3);
                p1.getFuncionalidades().add(f4);
                p1.getFuncionalidades().add(f5);
                
                Perfil p2 = new Perfil(null, "CORDENADOR", null); 
                p2.getFuncionalidades().add(f2);
                p2.getFuncionalidades().add(f3);
                p2.getFuncionalidades().add(f5);
                
                Perfil p3 = new Perfil(null, "ATENDENTE",null);
                p3.getFuncionalidades().add(f1);
                p3.getFuncionalidades().add(f2);
                p3.getFuncionalidades().add(f3);
                
                perfilRepo.saveAll(Arrays.asList(p1,p2,p3));
                
                
                Usuario usr1 = new Usuario(null, "Alan de Jesus Lima", "ajlima", encode.encode("1234567"), "alan.makau@gmail.com", "XPTO)0098", lotacao1, Boolean.TRUE);
                usr1.getPerfil().add(p1);
                Usuario usr2 = new Usuario(null, "Carlos Eduardo", "ceduardo", encode.encode("1234567"), "carlos.eduardo@gmail.com", "XPYHJUE763", lotacao2, Boolean.TRUE);
                usr2.getPerfil().add(p2);
                usr2.getPerfil().add(p3);
                Usuario usr3 = new Usuario(null, "Luiz Carlos Santos", "lcarlos", encode.encode("1234567"), "luiz.carlos@gmail.com", "XPJAH092", lotacao1, Boolean.TRUE);
                usr3.getPerfil().add(p1);
                usr3.getPerfil().add(p2);
                usr3.getPerfil().add(p3);
                Usuario usr4 = new Usuario(null, "Mateus santos dias", "mdias", encode.encode("1234567"), "mateus.dias@gmail.com", "XPJU73JHDD", lotacao3, Boolean.TRUE);
                usr4.getPerfil().add(p2);
                usr4.getPerfil().add(p3);
                
                usuarioRepo.saveAll(Arrays.asList(usr1,usr2,usr3,usr4));
                
        }
}
        
