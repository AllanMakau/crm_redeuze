/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Cliente;
import br.com.redeuze.crm.repository.ClienteRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */
@Service
public class ClienteService {
    
     
    @Autowired
    private ClienteRepository repo;
    
    
    
    public List<Cliente> obterLista(){
        return repo.findAll();
    }
    
    public Cliente obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Cliente obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Cliente obj){
        repo.save(obj);
    }
    
    public void excluir( Cliente obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
    
    public Cliente obterPorCpf(String cpf){
        return repo.findByCpf(cpf);
    }
    
    public Cliente obterPorNome(String nome){
        return repo.findByNome(nome);
    }
}
