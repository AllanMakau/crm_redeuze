/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Lotacao;
import br.com.redeuze.crm.repository.LotacaoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */
@Service
public class LotacaoService {
    
    
    @Autowired
    private LotacaoRepository  repo;
    
    
    
    public List<Lotacao> obterLista(){
        return repo.findAll();
    }
    
    public Lotacao obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Lotacao obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Lotacao obj){
        repo.save(obj);
    }
    
    public void excluir( Lotacao obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
}
