/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Ocorrencia;
import br.com.redeuze.crm.repository.OcorrenciaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */
@Service
public class OcorrenciaService {
    
    
     
    @Autowired
    private OcorrenciaRepository repo;
    
    
    
    public List<Ocorrencia> obterLista(){
        return repo.findAll();
    }
    
    public Ocorrencia obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Ocorrencia obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Ocorrencia obj){
        repo.save(obj);
    }
    
    public void excluir( Ocorrencia obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
    
    public Ocorrencia obterPorProtocolo(String protocolo){
        return repo.findByProtocolo(protocolo);
    }
}
