/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Usuario;
import br.com.redeuze.crm.repository.UsuarioRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */

@Service
public class UsuarioService {
    
    
    @Autowired
    private UsuarioRepository repo;
    
    
    
    public List<Usuario> obterLista(){
        return repo.findAll();
    }
    
    public Usuario obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Usuario obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Usuario obj){
        repo.save(obj);
    }
    
    public void excluir( Usuario obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
    
    public Usuario obterPorNome(String nome){
        return repo.findByNome(nome);
    }
    
    public Usuario obterPorLogin(String login){
        return repo.findByLogin(login);
    }
    
    public Usuario obterPorEmail(String email){
        return repo.findByEmail(email);
    }
}
