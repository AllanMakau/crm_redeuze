/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Perfil;
import br.com.redeuze.crm.repository.PerfilRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */
@Service
public class PerfilService {
    
    
    
    @Autowired
    private PerfilRepository repo;
    
    
    
    public List<Perfil> obterLista(){
        return repo.findAll();
    }
    
    public Perfil obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Perfil obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Perfil obj){
        repo.save(obj);
    }
    
    public void excluir( Perfil obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
}
