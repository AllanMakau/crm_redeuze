/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Rede;
import br.com.redeuze.crm.repository.RedeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */

@Service
public class RedeService {
    
    
    @Autowired
    private RedeRepository repo;
    
    
    
    public List<Rede> obterLista(){
        return repo.findAll();
    }
    
    public Rede obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Rede obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Rede obj){
        repo.save(obj);
    }
    
    public void excluir( Rede obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
}
