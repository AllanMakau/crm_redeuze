/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Funcionalidade;
import br.com.redeuze.crm.entity.Usuario;
import br.com.redeuze.crm.repository.UsuarioRepository;
import br.com.redeuze.crm.security.UserSS;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */


@Service
public class UserDetailServiceImpl implements UserDetailsService{

    @Autowired
    private UsuarioRepository repo;
    

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Usuario cli = repo.findByLogin(login);
		if (cli == null) {
			throw new UsernameNotFoundException(login);
		}
		return new UserSS(cli.getId(), cli.getLogin(), cli.getSenha(), cli.getPerfil());
	}
    
}
