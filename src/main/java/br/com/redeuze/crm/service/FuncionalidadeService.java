/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Funcionalidade;
import br.com.redeuze.crm.repository.FuncionalidadeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */
@Service
public class FuncionalidadeService {
    
    
    @Autowired
    private FuncionalidadeRepository repo;
    
    
    
    public List<Funcionalidade> obterLista(){
        return repo.findAll();
    }
    
    public Funcionalidade obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Funcionalidade obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Funcionalidade obj){
        repo.save(obj);
    }
    
    public void excluir( Funcionalidade obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
}
