/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Status;
import br.com.redeuze.crm.repository.StatusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */

@Service
public class StatusService {
    
    
    @Autowired
    private StatusRepository repo;
    
    
    
    public List<Status> obterLista(){
        return repo.findAll();
    }
    
    public Status obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Status obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Status obj){
        repo.save(obj);
    }
    
    public void excluir( Status obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
}
