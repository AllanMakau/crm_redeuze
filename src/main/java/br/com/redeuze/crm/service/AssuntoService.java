/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Assunto;
import br.com.redeuze.crm.repository.AssuntoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */
@Service
public class AssuntoService {
    
    
     
    @Autowired
    private AssuntoRepository repo;
    
    
    
    public List<Assunto> obterLista(){
        return repo.findAll();
    }
    
    public Assunto obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Assunto obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Assunto obj){
        repo.save(obj);
    }
    
    public void excluir( Assunto obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
        repo.deleteById(Long.parseLong(id));
    }
}
