/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.service;

import br.com.redeuze.crm.entity.Providencia;
import br.com.redeuze.crm.repository.ProvidenciaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */
@Service
public class ProvidenciaService {
    
    @Autowired
    private ProvidenciaRepository repo;
    
    
    
    public List<Providencia> obterLista(){
        return repo.findAll();
    }
    
    public Providencia obterPorId(String id){
        return repo.getOne(Long.parseLong(id));
    }
    
    public void cadastrar(Providencia obj){
        obj.setId(null);
        repo.save(obj);
    }
    
    public void atualizar(Providencia obj){
        repo.save(obj);
    }
    
    public void excluir( Providencia obj){    
        repo.delete(obj);
    }
       
    public void excluirPorId(String id){
    	repo.deleteById(Long.parseLong(id));
    }
}
