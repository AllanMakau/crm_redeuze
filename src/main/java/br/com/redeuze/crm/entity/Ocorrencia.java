/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Alan
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Ocorrencia implements Serializable{
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String protocolo;
    private Date data;
    private String texto;
    private Assunto assunto;
    private Usuario quemAbriu;
    private Cliente cliente;
    private Rede rede;
    private Status status;
    private Usuario quemTrata;
    @OneToMany
    private List<Providencia> providencias = new ArrayList<>();

    public Ocorrencia() {
    }

    public Ocorrencia(Long id, String protocolo, Date data, String texto, Assunto assunto, Usuario quemAbriu, Cliente cliente, Rede rede, Status status, Usuario quemTrata) {
        this.id = id;
        this.protocolo = protocolo;
        this.data = data;
        this.texto = texto;
        this.assunto = assunto;
        this.quemAbriu = quemAbriu;
        this.cliente = cliente;
        this.rede = rede;
        this.status = status;
        this.quemTrata = quemTrata;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Assunto getAssunto() {
        return assunto;
    }

    public void setAssunto(Assunto assunto) {
        this.assunto = assunto;
    }

    public Usuario getQuemAbriu() {
        return quemAbriu;
    }

    public void setQuemAbriu(Usuario quemAbriu) {
        this.quemAbriu = quemAbriu;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Rede getRede() {
        return rede;
    }

    public void setRede(Rede rede) {
        this.rede = rede;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Usuario getQuemTrata() {
        return quemTrata;
    }

    public void setQuemTrata(Usuario quemTrata) {
        this.quemTrata = quemTrata;
    }

    public List<Providencia> getProvidencias() {
        return providencias;
    }

    public void setProvidencias(List<Providencia> providencias) {
        this.providencias = providencias;
    }
    
    
    
    
}
