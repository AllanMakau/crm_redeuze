/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Alan
 */

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Providencia implements Serializable{
    
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date data;
    private String texto;
    private Usuario usuario;
    private Boolean conclusiva;

    public Providencia() {
    }

    public Providencia(Long id, Date data, String texto, Usuario usuario, Boolean conclusiva) {
        this.id = id;
        this.data = data;
        this.texto = texto;
        this.usuario = usuario;
        this.conclusiva = conclusiva;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Boolean getConclusiva() {
        return conclusiva;
    }

    public void setConclusiva(Boolean conclusiva) {
        this.conclusiva = conclusiva;
    }
    
    
}
