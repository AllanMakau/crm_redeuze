package br.com.redeuze.crm.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;



/**
 *
 * @author Alan
 */
@Entity
public class Perfil  implements  Serializable{
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String descricao;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "PerfilFuncionalidade",
    joinColumns =
    @JoinColumn(name = "id_perfil"),
    inverseJoinColumns =
    @JoinColumn(name = "id_funcionalidade"))
    @JsonBackReference
    private Set<Funcionalidade> funcionalidades = new HashSet<Funcionalidade>();
    
     @ManyToMany(mappedBy = "perfil",fetch = FetchType.EAGER)
     @JsonBackReference
    private Set<Usuario> usuario = new HashSet<Usuario>();
    
    
    public Perfil() {
        
    }

    public Perfil(Long id, String descricao, List<Funcionalidade> funcionalidades) {
        this.id = id;
        this.descricao = descricao;
        this.funcionalidades = this.funcionalidades;
       
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Funcionalidade> getFuncionalidades() {
        return funcionalidades;
    }

    public void setFuncionalidades(Set<Funcionalidade> funcionalidades) {
        this.funcionalidades = funcionalidades;
    }
    public Set<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(Set<Usuario> usuario) {
        this.usuario = usuario;
    }

   
    

    
    
}
