/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.controller;


import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan
 */
@RestController
@RequestMapping(value = "/swagger")
public class SwaggerController {
    
    
    @RequestMapping(method = RequestMethod.GET)
	public String retornaSwaggerIndex(HttpServletResponse response){
		
            
        return response.encodeRedirectURL("swagger-ui.html");
	}
    
}
