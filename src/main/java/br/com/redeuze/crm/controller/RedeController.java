/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.controller;

import br.com.redeuze.crm.entity.Rede;
import br.com.redeuze.crm.service.RedeService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


/**
 *
 * @author Alan
 */

@RestController
@RequestMapping(value = "/redes")
//@Api(tags = "Rede")
public class RedeController {
    
    @Autowired
    private RedeService service;
    
    
       // @ApiOperation(value="Listas Todos os Redes")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Rede>> obterLista(){
		
            List<Rede> objcts =service.obterLista();
        return ResponseEntity.ok(objcts);
	}
	
	
	
	
	//@ApiOperation(value="Obter Rede Por Id")
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	public ResponseEntity<Rede> obterPorId(@PathVariable String id){
		
            Rede obj = service.obterPorId(id);
        return ResponseEntity.ok(obj);
	}
	
	
	
	
	//@ApiOperation(value="Cadastrar uma Rede")
	@RequestMapping( method = RequestMethod.POST)
	public ResponseEntity<Rede> cadastrar(@RequestBody Rede obj){
		
            service.cadastrar(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
	}
	
	
	
	
	//@ApiOperation(value="Atualizar uma Rede")
	@RequestMapping( method = RequestMethod.PUT)
	public ResponseEntity<Rede> atualizar(@RequestBody Rede obj){
		
            service.cadastrar(obj);
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar uma Rede Por Id")
	@RequestMapping( value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizarPorId(@PathVariable String id){
		
            service.cadastrar(service.obterPorId(id));
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar uma Rede")
	@RequestMapping( method = RequestMethod.DELETE)
	public ResponseEntity<Rede> excluirPorObj(Rede obj){
		
            service.excluir(obj);
        return ResponseEntity.ok(obj);
	}
}
