/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.controller;

import br.com.redeuze.crm.entity.Perfil;
import br.com.redeuze.crm.service.PerfilService;
//import io.swagger.annotations.Api;
///import io.swagger.annotations.ApiOperation;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Alan
 */

@RestController
@RequestMapping(value = "/perfis")
//@Api(tags = "Perfil")
public class PerfilController {
    
    @Autowired
    private PerfilService service;
    
    
     //   @ApiOperation(value="Listas Todos os Perfis")
    
    @PreAuthorize(value = "hasRole('cadastro')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Perfil>> obterLista(){
		
            List<Perfil> objcts =service.obterLista();
        return ResponseEntity.ok(objcts);
	}
	
	
	
	
	//@ApiOperation(value="Obter Status Por Id")
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	public ResponseEntity<Perfil> obterPorId(@PathVariable String id){
		
            Perfil obj = service.obterPorId(id);
        return ResponseEntity.ok(obj);
	}
	
	
	
	
	//@ApiOperation(value="Cadastrar um Status")
	@RequestMapping( method = RequestMethod.POST)
	public ResponseEntity<Perfil> cadastrar(@RequestBody Perfil obj){
		
            service.cadastrar(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
	}
	
	
	
	
	//@ApiOperation(value="Atualizar um Status")
	@RequestMapping( method = RequestMethod.PUT)
	public ResponseEntity<Perfil> atualizar(@RequestBody Perfil obj){
		
            service.cadastrar(obj);
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar um Status Por Id")
	@RequestMapping( value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizarPorId(@PathVariable String id){
		
            service.cadastrar(service.obterPorId(id));
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar um Status")
	@RequestMapping( method = RequestMethod.DELETE)
	public ResponseEntity<Perfil> excluirPorObj(Perfil obj){
		
            service.excluir(obj);
        return ResponseEntity.ok(obj);
	}
}
