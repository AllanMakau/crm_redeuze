/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.controller;

import br.com.redeuze.crm.entity.Status;
import br.com.redeuze.crm.service.StatusService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Alan
 */

@RestController
@RequestMapping(value = "/status")
//@Api(tags = "Status")
public class StatusController {
    
    @Autowired
    private StatusService service;
    
    
    //    @ApiOperation(value="Listas Todos os Status")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Status>> obterLista(){
		
            List<Status> objcts =service.obterLista();
        return ResponseEntity.ok(objcts);
	}
	
	
	
	
	//@ApiOperation(value="Obter Status Por Id")
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	public ResponseEntity<Status> obterPorId(@PathVariable String id){
		
            Status obj = service.obterPorId(id);
        return ResponseEntity.ok(obj);
	}
	
	
	
	
	//@ApiOperation(value="Cadastrar um Status")
	@RequestMapping( method = RequestMethod.POST)
	public ResponseEntity<Status> cadastrar(@RequestBody Status obj){
		
            service.cadastrar(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
	}
	
	
	
	
	//@ApiOperation(value="Atualizar um Status")
	@RequestMapping( method = RequestMethod.PUT)
	public ResponseEntity<Status> atualizar(@RequestBody Status obj){
		
            service.cadastrar(obj);
        return ResponseEntity.noContent().build();
	}
	
	
	
	
//	@ApiOperation(value="Deletar um Status Por Id")
	@RequestMapping( value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizarPorId(@PathVariable String id){
		
            service.cadastrar(service.obterPorId(id));
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar um Status")
	@RequestMapping( method = RequestMethod.DELETE)
	public ResponseEntity<Status> excluirPorObj(Status obj){
		
            service.excluir(obj);
        return ResponseEntity.ok(obj);
	}
    
}
