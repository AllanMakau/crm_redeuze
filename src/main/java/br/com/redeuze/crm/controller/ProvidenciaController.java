/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.controller;

import br.com.redeuze.crm.entity.Providencia;
import br.com.redeuze.crm.service.ProvidenciaService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Alan
 */

@RestController
@RequestMapping(value = "/providencias")
//@Api(tags = "Providencia")
public class ProvidenciaController {
    
    
    @Autowired
    private ProvidenciaService service;
    
    
     //   @ApiOperation(value="Listas Todas os lotações")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Providencia>> obterLista(){
		
            List<Providencia> objcts =service.obterLista();
        return ResponseEntity.ok(objcts);
	}
	
	
	
	
	//@ApiOperation(value="Obter Lotação Por Id")
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	public ResponseEntity<Providencia> obterPorId(@PathVariable String id){
		
            Providencia obj = service.obterPorId(id);
        return ResponseEntity.ok(obj);
	}
	
	
	
	
	//@ApiOperation(value="Cadastrar uma Lotação")
	@RequestMapping( method = RequestMethod.POST)
	public ResponseEntity<Providencia> cadastrar(@RequestBody Providencia obj){
		
            service.cadastrar(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
	}
	
	
	
	
	//@ApiOperation(value="Atualizar uma Lotação")
	@RequestMapping( method = RequestMethod.PUT)
	public ResponseEntity<Providencia> atualizar(@RequestBody Providencia obj){
		
            service.cadastrar(obj);
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar uma Lotação Por Id")
	@RequestMapping( value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizarPorId(@PathVariable String id){
		
            service.cadastrar(service.obterPorId(id));
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar uma Lotação")
	@RequestMapping( method = RequestMethod.DELETE)
	public ResponseEntity<Providencia> excluirPorObj(Providencia obj){
		
            service.excluir(obj);
        return ResponseEntity.ok(obj);
	}
    
}
