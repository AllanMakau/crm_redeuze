/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.controller;

import br.com.redeuze.crm.entity.Funcionalidade;
import br.com.redeuze.crm.service.FuncionalidadeService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Alan
 */

@RestController
@RequestMapping(value = "/funcionalidades")
//@Api(tags = "Funcionalidade")
public class FuncionalidadeController {
    

    @Autowired
    private FuncionalidadeService service;
    
    
      //  @ApiOperation(value="Listas Todos as Funcionalidades")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Funcionalidade>> obterLista(){
		
            List<Funcionalidade> objcts =service.obterLista();
        return ResponseEntity.ok(objcts);
	}
	

        
        
	//@ApiOperation(value="Obter Funcionalidade Por Id")
	
	
	

	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	public ResponseEntity<Funcionalidade> obterPorId(@PathVariable String id){
		
            Funcionalidade obj = service.obterPorId(id);
        return ResponseEntity.ok(obj);
	}
	

	
	
	//@ApiOperation(value="Cadastrar uma Funcionalidade")
	@PreAuthorize("hasAnyRole('cadastro')")
	@RequestMapping( method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Funcionalidade> cadastrar(@RequestBody Funcionalidade obj){
		
            service.cadastrar(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
	}
	
	
	
	
	//@ApiOperation(value="Atualizar uma Funcionalidade")
	@PreAuthorize("hasAnyRole('cadastro')")
	@RequestMapping( method = RequestMethod.PUT)
	public ResponseEntity<Funcionalidade> atualizar(@RequestBody Funcionalidade obj){
		
            service.cadastrar(obj);
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar uma Funcionalidade Por Id")
	@PreAuthorize("hasAnyRole('cadastro')")
	@RequestMapping( value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizarPorId(@PathVariable String id){
		
            service.cadastrar(service.obterPorId(id));
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar uma Funcionalidade")
	@PreAuthorize("hasAnyRole('cadastro')")
	@RequestMapping( method = RequestMethod.DELETE)
	public ResponseEntity<Funcionalidade> excluirPorObj(Funcionalidade obj){
		
            service.excluir(obj);
        return ResponseEntity.ok(obj);
	}
}
