/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.redeuze.crm.controller;

import br.com.redeuze.crm.entity.Assunto;
import br.com.redeuze.crm.service.AssuntoService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Alan
 */

@RestController
@RequestMapping(value = "/assuntos")
///@Api(tags = "Assunto")
public class AssuntoController {
    
     @Autowired
    private AssuntoService service;
   
    
       // @ApiOperation(value="Listas Todas os Assunto")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Assunto>> obterLista(){
		
            List<Assunto> objcts =service.obterLista();
        return ResponseEntity.ok(objcts);
	}
	
	
	
	
	//@ApiOperation(value="Obter Assunto Por Id")
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	public ResponseEntity<Assunto> obterPorId(@PathVariable String id){
		
            Assunto obj = service.obterPorId(id);
        return ResponseEntity.ok(obj);
	}
	
	
	
	
	//@ApiOperation(value="Cadastrar um Assunto")
	@RequestMapping( method = RequestMethod.POST)
	public ResponseEntity<Assunto> cadastrar(@RequestBody Assunto obj){
		
            service.cadastrar(obj);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
	}
	
	
	
	
	//@ApiOperation(value="Atualizar um Assunto")
	@RequestMapping( method = RequestMethod.PUT)
	public ResponseEntity<Assunto> atualizar(@RequestBody Assunto obj){
		
            service.cadastrar(obj);
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar um Assunto Por Id")
	@RequestMapping( value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizarPorId(@PathVariable String id){
		
            service.cadastrar(service.obterPorId(id));
        return ResponseEntity.noContent().build();
	}
	
	
	
	
	//@ApiOperation(value="Deletar um Assunto")
	@RequestMapping( method = RequestMethod.DELETE)
	public ResponseEntity<Assunto> excluirPorObj(Assunto obj){
		
            service.excluir(obj);
        return ResponseEntity.ok(obj);
	}
}
